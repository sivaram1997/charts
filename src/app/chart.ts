
export const ChartOptions={

    xAxis: {
        categories: ['2011','2012','2013','2014','2015','2016', '2017'],
        title: {
          text:'list of years'
        }
      },
      yAxis: {
        min: 0
        
    },
    series: [{
        name:'IND-Money',
        data: [],
    }],
     chart: {
plotBackgroundColor: null,
plotBorderWidth: null,
plotShadow: false,
type: ''
},
tooltip: {
pointFormat: '{series.name}: <b>{point.y:,.0f} </b> <b>{point.percentage:.1f}%</b>'
},
plotOptions: {
pie: {
    
allowPointSelect: true,
cursor: 'pointer',
dataLabels: {
enabled: true,
format: '<b>{point.name}</b>: {point.percentage:.1f} %'
}
}
}
    }
